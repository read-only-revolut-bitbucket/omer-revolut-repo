package revolut.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import revolut.entity.Account;
import revolut.manager.AccountManager;
import revolut.session.AccountRepository;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public class AccountController {

  public static final Logger logger = LogManager.getLogger(AccountController.class);

  public AccountController() {
  }

  static AccountController instace;

  synchronized static public void initInstance(){
    instace = new AccountController();
  }

  public static AccountController getInstace() {
    return instace;
  }

  public Account getAccount(Integer id){

    return AccountRepository.getInstace().getAccount(id);
  }

  public void transfer(int sendAccountId, int receiveAccountId) {

    logger.debug(String.format("transfer money from %d to %d.", sendAccountId, receiveAccountId));

    Account sendAccount = getAccount(sendAccountId);
    Account receiveAccount = getAccount(receiveAccountId);

    if(sendAccountId == receiveAccountId){
      logger.warn(String.format("Same account cannot send to itself (accountId %d).", sendAccountId));
      throw new IllegalArgumentException("Same account cannot send to itself.");
    }

    transfer(sendAccount, receiveAccount);
  }

  public void transfer(Account sendAccount, Account receiveAccount) {

    AccountManager.getInstace().transfer(sendAccount, receiveAccount);
  }
}