package revolut.manager;

import java.math.BigDecimal;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import revolut.entity.Account;
import revolut.exception.RevolutNotEnoughBalanceException;
import revolut.session.AccountRepository;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public class AccountManager {


  public static final Logger logger = LogManager.getLogger(AccountManager.class);

  static AccountManager instace;

  synchronized static public void initInstance(){
    instace = new AccountManager();
  }

  public static AccountManager getInstace() {
    return instace;
  }

  synchronized public void transfer(Account sendAccount, Account receiveAccount) {

    logger.debug(String.format("transfer(synch) from %d to %d.", sendAccount.getId(), receiveAccount.getId()));

    sendAccount = find(sendAccount.getId());
    receiveAccount = find(receiveAccount.getId());

    persistedValidations(sendAccount, receiveAccount);

    doTransfer(sendAccount, receiveAccount);
  }

  private void persistedValidations(Account sendAccount, Account receiveAccount) {

    if(!sendAccount.hasEnoughAmount()){
      throw new RevolutNotEnoughBalanceException(String.format("Sender Account does not have enough balance (amount: %s)", sendAccount.getAmount()));
    }
  }

  //@Transactional
  public void doTransfer(Account sendAccount, Account receiveAccount){

    logger.debug(String.format("doTransfer(synch & transactional) from %d to %d.", sendAccount.getId(), receiveAccount.getId()));

    beginTransaction(sendAccount, receiveAccount);
    try {

      sendAccount.send(new BigDecimal(17));
      receiveAccount.receive(new BigDecimal(17));

      AccountRepository.getInstace().addAccount(sendAccount);
      Boolean isSenderBlocked = checkIfSenderChargeIsBlocked();
      if (isSenderBlocked) {
        throw new IllegalArgumentException("Failed to block charge from Sender Account.");
      }
      AccountRepository.getInstace().addAccount(sendAccount);
    }catch (IllegalArgumentException e){
      rollback(sendAccount, receiveAccount);
      throw e;

    }
  }

  // To show rollback is done
  Account rollbackSendAccount = new Account();
  Account rollbackReceiveAccount = new Account();
  private void beginTransaction(Account sendAccount, Account receiveAccount) {
    this.rollbackSendAccount.setAmount(sendAccount.getAmount());
    this.rollbackReceiveAccount.setAmount(receiveAccount.getAmount());
  }
  public void rollback(Account sendAccount, Account receiveAccount) {
    sendAccount.setAmount(this.rollbackSendAccount.getAmount());
    receiveAccount.setAmount(this.rollbackReceiveAccount.getAmount());
  }

  public Boolean checkIfSenderChargeIsBlocked() {
    return false;
  }

  public Account find(Integer accountId){
    return AccountRepository.getInstace().getAccount(accountId);
  }

  public void remove(Integer accountId){
    AccountRepository.getInstace().removeAccount(accountId);
  }

  public void removeAll(){
    AccountRepository.getInstace().removeAllAccounts();
  }

}
