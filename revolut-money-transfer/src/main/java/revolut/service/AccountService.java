package revolut.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import revolut.entity.Account;
import revolut.session.AccountRepository;
import revolut.controller.AccountController;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
@Path("/account")
public class AccountService {

  @GET
  @Path("/transfer/{sendAccountId}/{receiveAccountId}")
  public Boolean transfer(@PathParam("sendAccountId") Integer sendAccountId, @PathParam("receiveAccountId") Integer receiveAccountId) {

    Map<Integer, Account> accountList = new ConcurrentHashMap<>(5);
    accountList.put(1, new Account(1, new BigDecimal(1000000)));
    accountList.put(2, new Account(2, new BigDecimal(6000)));
    accountList.put(3, new Account(3, new BigDecimal(9000)));
    accountList.put(4, new Account(4, new BigDecimal(7000000)));
    accountList.put(5, new Account(5, new BigDecimal(8000)));
    AccountRepository.getInstace().setAccountList(accountList);

    AccountController.getInstace().transfer(sendAccountId, receiveAccountId);

    return Boolean.TRUE;
  }

  @GET
  @Path("/add/{accountId}/{amount}")
  public Boolean add(@PathParam("accountId") Integer accountId, @PathParam("amount") BigDecimal amount) {

    AccountRepository.getInstace().addAccount(new Account(accountId, amount));

    return Boolean.TRUE;
  }

  @GET
  @Path("/remove/{accountId}")
  public Boolean remove(@PathParam("accountId") Integer accountId) {

    AccountRepository.getInstace().removeAccount(accountId);

    return Boolean.TRUE;
  }

  @GET
  @Path("/removeallaccounts")
  public Boolean removeallaccounts() {

    AccountRepository.getInstace().removeAllAccounts();

    return Boolean.TRUE;
  }

  @GET
  @Path("/addaccount/{accountId}/{amount}")
  public Account addAccount(@PathParam("accountId") Integer accountId, @PathParam("amount") BigDecimal amount) {

    Account account = new Account(accountId, amount);
    AccountRepository.getInstace().addAccount(account);

    return account;
  }

  @POST
  @Path("/addaccountobject")
  @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public Response addAccount(Account account) {

    AccountRepository.getInstace().addAccount(account);

    return Response.ok().entity(account).build();
  }

  @POST
  @Path("/addaccountmap")
  @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public Response addAccountMap(Map<Integer, Account> accountList) {

    AccountRepository.getInstace().setAccountList(accountList);

    return Response.ok().entity(accountList).build();
  }

  @POST
  @Path("/addaccountlist")
  @Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
  public Response addAccountList(List<Account> accountList) {

    for (Account account : accountList) {
      AccountRepository.getInstace().addAccount(new Account(account.getId(), account.getAmount()));
    }

    return Response.ok().entity(accountList).build();
  }

}
