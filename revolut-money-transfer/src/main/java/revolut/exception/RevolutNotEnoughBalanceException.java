package revolut.exception;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public class RevolutNotEnoughBalanceException extends RuntimeException {

  public RevolutNotEnoughBalanceException() {
  }

  public RevolutNotEnoughBalanceException(String message) {
    super(message);
  }

  public RevolutNotEnoughBalanceException(String message, Throwable cause) {
    super(message, cause);
  }

  public RevolutNotEnoughBalanceException(Throwable cause) {
    super(cause);
  }

  public RevolutNotEnoughBalanceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
