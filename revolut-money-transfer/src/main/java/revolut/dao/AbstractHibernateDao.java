package revolut.dao;

import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public abstract class AbstractHibernateDao<T extends Serializable> {

  private Class<T> clazz;
  @Inject
  SessionFactory sessionFactory;

  public void setClazz(Class<T> clazzToSet) {
    this.clazz = clazzToSet;
  }

  public T findOne(long id) {
    return (T) getCurrentSession().get(clazz, id);
  }

  public List findAll() {
    return getCurrentSession().createQuery("from " + clazz.getName()).list();
  }

  public T create(T entity) {
    getCurrentSession().saveOrUpdate(entity);
    return entity;
  }

  public T update(T entity) {
    return (T) getCurrentSession().merge(entity);
  }

  public void delete(T entity) {
    getCurrentSession().delete(entity);
  }

  public void deleteById(long entityId) {
    T entity = findOne(entityId);
    delete(entity);
  }

  protected Session getCurrentSession() {
    return sessionFactory.getCurrentSession();
  }
}
