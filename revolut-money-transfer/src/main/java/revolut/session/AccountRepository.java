package revolut.session;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import revolut.entity.Account;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public class AccountRepository {

  public static final Logger logger = LogManager.getLogger(AccountRepository.class);
  static AccountRepository instace;
  private Map<Integer, Account> accountList = new ConcurrentHashMap<>();

  synchronized static public void initInstance() {
    instace = new AccountRepository();
  }

  public static AccountRepository getInstace() {
    return instace;
  }

  public void setAccountList(Map<Integer, Account> accountList) {
    this.accountList = accountList;
  }

  public void addAccount(Account account){
    this.accountList.put(account.getId(), account);
  }

  public void removeAccount(Integer accountdId){
    this.accountList.remove(accountdId);
  }

  public void removeAllAccounts(){
    this.accountList.clear();
  }

  public Map<Integer, Account> getAccountList() {
    return accountList;
  }

  public Account getAccount(Integer id) {
    return accountList.get(id);
  }
}
