package revolut.entity;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.Id;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
@Entity
public class Account {

  public static final Logger logger = LogManager.getLogger(Account.class);

  @Id
  private Integer id;
  private BigDecimal amount;
  private BigDecimal initialAmount;

  public Account() {
  }

  public Account(Integer id, BigDecimal amount) {
    this.id = id;
    this.amount = amount;
    this.initialAmount = amount;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public BigDecimal getAmount() {
    return amount;
  }

  public void setAmount(BigDecimal amount) {
    this.amount = amount;
  }

  public BigDecimal getInitialAmount() {
    return initialAmount;
  }

  public Boolean hasEnoughAmount(){

    if( getAmount().subtract(new BigDecimal(17)).compareTo(BigDecimal.ZERO) == -1 )
      return false;

    return true;
  }

  // sendMoney::reduce::withdraw
  public void send(BigDecimal amount){
    this.amount = this.amount.subtract(amount);
  }

  // receiveMoney::increase::deposit
  public void receive(BigDecimal amount){
    this.amount = this.amount.add(amount);
  }

  public Boolean isValid(BigDecimal inSendReceiveDiffSize){

    BigDecimal sendReceiveDiff = getAmount().subtract(getInitialAmount());
    BigDecimal sendReceiveDiffSize = sendReceiveDiff.divide(new BigDecimal(17));

    Boolean valid = false;
    if(sendReceiveDiffSize.compareTo(inSendReceiveDiffSize) == 0)
      valid = true;

    logger.debug("ID: " + getId() + ", Valid: " + valid + ", Amount: " + getAmount() + ", InitialAmount: " + getInitialAmount() + ", sendReceiveDiff: " + sendReceiveDiffSize + ", inSendReceiveDiffSize: " + inSendReceiveDiffSize);

    return valid;
  }

  @Override
  public String toString() {
    return "Account id: " + this.getId() + ", amount: " + this.getAmount();
  }

  @Override
  public boolean equals(Object obj) {
    return this.getId().equals(((Account)obj).getId());
  }
}
