package vls.demo;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import revolut.controller.AccountController;
import revolut.manager.AccountManager;
import revolut.session.AccountRepository;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
@WebListener
public class ApplicationStartUpListener implements ServletContextListener {

  @Override
  public void contextInitialized(ServletContextEvent event) {

    AccountController.initInstance();
    AccountManager.initInstance();
    AccountRepository.initInstance();
  }

  @Override
  public void contextDestroyed(ServletContextEvent event) {

  }
}