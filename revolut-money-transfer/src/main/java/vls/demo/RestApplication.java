package vls.demo;

import revolut.controller.AccountController;
import revolut.manager.AccountManager;
import revolut.session.AccountRepository;

/**
 * Hello world!
 *
 */
public class RestApplication
{
    public static void main( String[] args )
    {
        AccountController.initInstance();
        AccountManager.initInstance();
        AccountRepository.initInstance();
    }

    public String hello() {
        return "Welcome to Revolut!";
    }
}
