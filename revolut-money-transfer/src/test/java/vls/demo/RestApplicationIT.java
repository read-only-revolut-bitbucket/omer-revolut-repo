package vls.demo;

import java.io.IOException;
import javax.ws.rs.ApplicationPath;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
@ApplicationPath("api")
public class RestApplicationIT {

  @Test
  public void whenSendingGet_thenMessageIsReturned() throws IOException {

    Assertions.assertEquals("Welcome to Revolut!", new RestApplication().hello());
  }
}
