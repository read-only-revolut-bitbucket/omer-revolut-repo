package revolut.manager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;
import revolut.entity.Account;
import revolut.integration.mockdata.MockData;
import revolut.session.AccountRepository;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
public class AccountManagerTest {

  private static EntityManagerFactory emf;

  @BeforeClass
  public static void setupBeforeClass(){
    AccountManager.initInstance();
    AccountRepository.initInstance();

    emf = Persistence.createEntityManagerFactory("h2-persistence-unit");
  }

  @Before
  public void setupBefore(){
    AccountRepository.getInstace().removeAllAccounts();
    AccountRepository.getInstace().setAccountList(MockData.get5MockAccountMap());
  }

  @Test
  public void transfer(){

    Account sendAccount = AccountRepository.getInstace().getAccount(1);
    Account receiveAccount = AccountRepository.getInstace().getAccount(3);

    AccountManager.getInstace().transfer(sendAccount, receiveAccount);

    Assertions.assertEquals(sendAccount.getInitialAmount().subtract(new BigDecimal(17)), sendAccount.getAmount());
    Assertions.assertEquals(receiveAccount.getInitialAmount().add(new BigDecimal(17)), receiveAccount.getAmount());
  }

  @Test(expected = IllegalArgumentException.class)
  public void whenSenderChargeIsBlocked_thenException(){

    Account sendAccount = AccountRepository.getInstace().getAccount(1);
    Account receiveAccount = AccountRepository.getInstace().getAccount(3);

    BigDecimal sendAccountAmountBeforeTransaction = sendAccount.getAmount();

    AccountManager mockAccountManager = spy(AccountManager.class);
    Mockito.lenient().when(mockAccountManager.checkIfSenderChargeIsBlocked()).thenReturn(true);

    try{
      mockAccountManager.doTransfer(sendAccount, receiveAccount);
    }catch (IllegalArgumentException e){

      // Rollback control
      assertEquals(sendAccountAmountBeforeTransaction, sendAccount.getAmount());

      throw e;
    }
  }

  @Test(expected = IllegalArgumentException.class)
  public void whenSenderChargeIsBlockedAndCouldNotRollback_ThenCheckCouldNotRollbackAndException(){

    Account sendAccount = AccountRepository.getInstace().getAccount(1);
    Account receiveAccount = AccountRepository.getInstace().getAccount(3);

    BigDecimal sendAccountAmountBeforeTransaction = sendAccount.getAmount();

    AccountManager mockAccountManager = spy(AccountManager.class);
    Mockito.lenient().when(mockAccountManager.checkIfSenderChargeIsBlocked()).thenReturn(true);


    doAnswer(new Answer() {
      @Override
      public Object answer(InvocationOnMock invocation) throws Throwable {
        return null;
      }
    }).when(mockAccountManager).rollback(any(),any());

    try{
      mockAccountManager.doTransfer(sendAccount, receiveAccount);
    }catch (IllegalArgumentException e){

      // Could not Rollback control
      assertNotEquals(sendAccountAmountBeforeTransaction, sendAccount.getAmount());

      throw e;
    }
  }
}