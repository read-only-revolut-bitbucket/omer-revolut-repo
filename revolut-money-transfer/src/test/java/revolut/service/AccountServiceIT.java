package revolut.service;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import revolut.controller.AccountControllerIT;
import revolut.entity.Account;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 *
 * !Create fake objects on rest server on AccountSession instance.
 */
public class AccountServiceIT {

  public static final Logger logger = LogManager.getLogger(AccountControllerIT.class);
  private static Client client;
  private static WebTarget webTarget;
  public static final String REST_URI = "http://localhost:8999/api/account";

  @BeforeAll
  public static void setupBeforeAll(){

  }

  @BeforeEach
  public void setup() throws IOException {

    client = ClientBuilder.newClient();
    webTarget = client.target(REST_URI);

    Boolean result = (Boolean) getJsonObject("removeallaccounts", Boolean.class);
    Assertions.assertEquals(result.toString(), Boolean.TRUE.toString());
  }

  @Test
  public void whenAddAccountPost_thenAccountIsReturned() throws IOException {

    Account account = (Account) getJsonObject("addaccount/3/9000", Account.class);
    Assertions.assertEquals(account.getId(), new Integer(3));
  }

  @Test
  public void whenAddAccountObjectPost_thenAccountObjectReturned() throws IOException {

    Account account = new Account(4, new BigDecimal(20000));

    Response response = createJsonObject("addaccountobject", account);
    assertStatus(response);
    Assertions.assertEquals(response.readEntity(Account.class).getId(), account.getId());
  }

  @Test
  public void whenAddAccountListPost_thenAccountMapReturned() throws IOException {

    Map<Integer, Account> accountListMap = new ConcurrentHashMap<>(5);
    accountListMap.put(1, new Account(1, new BigDecimal(1000000)));
    accountListMap.put(2, new Account(2, new BigDecimal(6000)));
    accountListMap.put(3, new Account(3, new BigDecimal(9000)));
    accountListMap.put(4, new Account(4, new BigDecimal(7000000)));
    accountListMap.put(5, new Account(5, new BigDecimal(8000)));

    Response response = createJsonObject("addaccountmap", accountListMap);
    assertStatus(response);
  }

  @Test
  public void whenAddAccountListPost_thenAccountListReturned() throws IOException {

    List<Account> accountList = new CopyOnWriteArrayList<>();
    accountList.add(new Account(1, new BigDecimal(1000000)));
    accountList.add(new Account(2, new BigDecimal(6000)));
    accountList.add(new Account(3, new BigDecimal(9000)));
    accountList.add(new Account(4, new BigDecimal(7000000)));
    accountList.add(new Account(5, new BigDecimal(8000)));

    Response response = createJsonObject("addaccountlist", accountList);
    assertStatus(response);
  }

  private Response createJsonObject(String path, Object obj) {
    return webTarget.path(path).request(MediaType.APPLICATION_JSON).post(Entity.entity(obj, MediaType.APPLICATION_JSON));
  }

  private Object getJsonObject(String path, Class clz) {
    return webTarget.path(path).request(MediaType.APPLICATION_JSON).get(clz);
  }

  private void assertStatus(Response response) {
    Assertions.assertEquals(response.getStatus(), Status.OK.getStatusCode());
  }
  private void assertAccount(Account account1, Account account2) {
    Assertions.assertEquals(account1.getId(), account2.getId());
  }

  @Test
  public void whenAddAccountGet_thenTrueReturned() throws IOException {

    String url = "http://localhost:8999/api/account/add/3/9000";
    URLConnection connection = new URL(url).openConnection();
    try (InputStream response = connection.getInputStream();
        Scanner scanner = new Scanner(response)) {
      String responseBody = scanner.nextLine();
      Assertions.assertEquals(Boolean.TRUE.toString(), responseBody);
    }catch (Exception e){
      logger.error("whenAddAccountGet_thenTrueReturned Exception: ", e);
    }
  }
}
