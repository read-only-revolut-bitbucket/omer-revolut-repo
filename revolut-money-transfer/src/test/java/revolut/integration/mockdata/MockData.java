package revolut.integration.mockdata;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import revolut.controller.AccountControllerIT;
import revolut.entity.Account;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public class MockData {

  public static final Logger logger = LogManager.getLogger(AccountControllerIT.class);
  private static Client client;
  private static WebTarget webTarget;
  public static final String REST_URI = "http://localhost:8999/api/account";

  public static List<Account> get5MockAccountList(){

    List<Account> accountList = new CopyOnWriteArrayList<>();
    accountList.add(new Account(1, new BigDecimal(1000000)));
    accountList.add(new Account(2, new BigDecimal(6000)));
    accountList.add(new Account(3, new BigDecimal(9000)));
    accountList.add(new Account(4, new BigDecimal(7000000)));
    accountList.add(new Account(5, new BigDecimal(8000)));
    return accountList;
  }

  public static Map<Integer, Account> get5MockAccountMap() {

    Map<Integer, Account> accountListMap = new ConcurrentHashMap<>(5);
    accountListMap.put(1, new Account(1, new BigDecimal(1000000)));
    accountListMap.put(2, new Account(2, new BigDecimal(6000)));
    accountListMap.put(3, new Account(3, new BigDecimal(9000)));
    accountListMap.put(4, new Account(4, new BigDecimal(7000000)));
    accountListMap.put(5, new Account(5, new BigDecimal(8000)));
    return accountListMap;
  }

  public static void init5MockAccount(){

    client = ClientBuilder.newClient();
    webTarget = client.target(REST_URI);

    Response response = createJsonObject("addaccountlist", get5MockAccountList());
    assertStatus(response);
  }

  private static Response createJsonObject(String path, Object obj) {
    return webTarget.path(path).request(MediaType.APPLICATION_JSON).post(Entity.entity(obj, MediaType.APPLICATION_JSON));
  }

  private static void assertStatus(Response response) {
    Assertions.assertEquals(response.getStatus(), Status.OK.getStatusCode());
  }
}
