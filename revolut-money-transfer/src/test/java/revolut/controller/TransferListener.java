package revolut.controller;

import revolut.entity.Account;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public interface TransferListener {

  void onTransferStart(Account sendAccount, Account receiveAccount);

  void onTransferCompleted(Account sendAccount, Account receiveAccount);
}
