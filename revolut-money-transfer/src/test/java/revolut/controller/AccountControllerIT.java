package revolut.controller;

import static java.time.Duration.ofSeconds;
import static org.junit.jupiter.api.Assertions.assertTimeout;

import java.math.BigDecimal;
import java.util.concurrent.Executors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import revolut.entity.Account;
import revolut.integration.mockdata.MockData;
import revolut.manager.AccountManager;
import revolut.session.AccountRepository;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public class AccountControllerIT {

  public static final Logger logger = LogManager.getLogger(AccountControllerIT.class);
  private AccountControllerTransferMoneyExecutor accountControllerExecutor;

  @BeforeAll
  public static void setupBeforeAll(){
    AccountController.initInstance();
    AccountManager.initInstance();
    AccountRepository.initInstance();
  }

  @BeforeEach
  public void setup(){
    AccountRepository.getInstace().removeAllAccounts();
    AccountRepository.getInstace().setAccountList(MockData.get5MockAccountMap());
  }

   @Test
   public void whenTransferSucceed(){

     accountControllerExecutor = new revolut.controller.AccountControllerTransferMoneyExecutor(Executors.newSingleThreadExecutor());
     accountControllerExecutor.addListener(new AccountControllerTransferMoneyAssertions());
     accountControllerExecutor.transfer(5,6);
   }

   @Test
   public void whenSimultaneousTransfersAllThreadSafe(){

     accountControllerExecutor = new revolut.controller.AccountControllerTransferMoneyExecutor(Executors.newFixedThreadPool(2000));
     accountControllerExecutor.addListener(new revolut.controller.AccountControllerTransferMoneyAssertions());
     accountControllerExecutor.transferRandomList();
   }

  @Test
  public void whenTransfer_thenCheckAmounts(){

    AccountController.getInstace().transfer(3,5);

    Account sendAccount = AccountController.getInstace().getAccount(3);
    Assertions.assertEquals(sendAccount.getInitialAmount().subtract(new BigDecimal(17)), sendAccount.getAmount());

    Account receiveAccount = AccountController.getInstace().getAccount(5);
    Assertions.assertEquals(receiveAccount.getInitialAmount().add(new BigDecimal(17)), receiveAccount.getAmount());
  }

  @Test
  public void doTransferInASecond(){

    assertTimeout(ofSeconds(1), () -> {
      AccountController.getInstace().transfer(3,5);
    });
  }

  @Test
  public void transfer(){

    Account sendAccount = AccountRepository.getInstace().getAccount(1);
    Account receiveAccount = AccountRepository.getInstace().getAccount(3);

    AccountManager.getInstace().transfer(sendAccount, receiveAccount);

    Assertions.assertEquals(sendAccount.getInitialAmount().subtract(new BigDecimal(17)), sendAccount.getAmount());
    Assertions.assertEquals(receiveAccount.getInitialAmount().add(new BigDecimal(17)), receiveAccount.getAmount());

    Account persistentSendAccount = AccountManager.getInstace().find(sendAccount.getId());
    Account persistentReceiveAccount = AccountManager.getInstace().find(receiveAccount.getId());

    Assertions.assertEquals(persistentSendAccount.getInitialAmount().subtract(new BigDecimal(17)), persistentSendAccount.getAmount());
    Assertions.assertEquals(persistentReceiveAccount.getInitialAmount().add(new BigDecimal(17)), persistentReceiveAccount.getAmount());
  }
}