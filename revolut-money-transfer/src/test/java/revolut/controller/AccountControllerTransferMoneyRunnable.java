package revolut.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import revolut.entity.Account;
import revolut.manager.AccountManager;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public class AccountControllerTransferMoneyRunnable implements Runnable, TransferListener {

  public static final Logger logger = LogManager.getLogger(AccountControllerTransferMoneyRunnable.class);

  private Account sendAccount;
  private Account receiveAccount;
  private TransferListener transferListener;

  public AccountControllerTransferMoneyRunnable(Account sendAccount, Account receiveAccount, TransferListener transferListener) {
    this.sendAccount = sendAccount;
    this.receiveAccount = receiveAccount;
    this.transferListener = transferListener;
  }

  @Override
  public void onTransferStart(Account sendAccount, Account receiveAccount) {
    transferListener.onTransferStart(sendAccount, receiveAccount);
  }

  @Override
  public void onTransferCompleted(Account sendAccount, Account receiveAccount) {
    transferListener.onTransferCompleted(sendAccount, receiveAccount);
  }

  @Override
  public void run() {

    onTransferStart(sendAccount, receiveAccount);

    transfer();

    onTransferCompleted(sendAccount, receiveAccount);
  }

  private void transfer() {

    AccountManager.getInstace().transfer(sendAccount, receiveAccount);

    logger.debug(
        "Transfer:: " +
        sendAccount.getId()+">"+sendAccount.getAmount() + ", " +
        receiveAccount.getId()+">"+receiveAccount.getAmount());
  }
}
