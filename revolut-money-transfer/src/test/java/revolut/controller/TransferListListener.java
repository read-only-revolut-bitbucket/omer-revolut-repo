package revolut.controller;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public interface TransferListListener extends TransferListener {

  void onTransfersCompleted();

  void initMockData();
}
