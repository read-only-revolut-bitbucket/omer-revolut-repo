package revolut.controller;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import revolut.entity.Account;
import revolut.session.AccountRepository;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public class AccountControllerTransferMoneyAssertions implements TransferListListener {

  public static final Logger logger = LogManager.getLogger(AccountControllerIT.class);
  // Used for transfer size control
  private Map<Integer, BigDecimal> threadlessSendAndReceiveAccountsList;
  public static final int transferCount = 10000;

  public AccountControllerTransferMoneyAssertions() {
    initMockData();
  }

  @Override
  public void initMockData(){

    threadlessSendAndReceiveAccountsList = new ConcurrentHashMap<>();
    for (int i = 1; i <=transferCount; i++) {
      threadlessSendAndReceiveAccountsList.put(i, BigDecimal.ZERO);
    }
  }

  @Override
  public void onTransferStart(Account sendAccount, Account receiveAccount) {

    threadlessSendAndReceiveAccountsList.put(sendAccount.getId(), threadlessSendAndReceiveAccountsList.get(sendAccount.getId()).subtract(BigDecimal.ONE));
    threadlessSendAndReceiveAccountsList.put(receiveAccount.getId(), threadlessSendAndReceiveAccountsList.get(receiveAccount.getId()).add(BigDecimal.ONE));
  }

  @Override
  public void onTransferCompleted(Account sendAccount, Account receiveAccount) {

    Assertions.assertEquals(sendAccount.getInitialAmount().subtract(new BigDecimal(17)), sendAccount.getAmount());
    Assertions.assertEquals(receiveAccount.getInitialAmount().add(new BigDecimal(17)), receiveAccount.getAmount());
  }

  @Override
  public void onTransfersCompleted() {

    AtomicInteger invalidCount = new AtomicInteger();
    AtomicInteger index = new AtomicInteger();
    threadlessSendAndReceiveAccountsList.entrySet().forEach(entry->{

      Account account = AccountRepository.getInstace().getAccount(index.incrementAndGet());
      Boolean valid = account.isValid(entry.getValue());

      if (!valid) {
        invalidCount.incrementAndGet();
      }
    });

    if(invalidCount.get() > 0)
      logger.warn("invalidCount: " + invalidCount);
    else
      logger.debug("invalidCount: " + invalidCount);

    Assertions.assertEquals(invalidCount.get(), 0);
  }

}
