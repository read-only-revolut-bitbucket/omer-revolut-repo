package revolut.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import revolut.entity.Account;
import revolut.session.AccountRepository;

/**
 * @author omerbatmaz@gmail.com Jun 2019
 */
public class AccountControllerTransferMoneyExecutor implements TransferListListener {

  public static final Logger logger = LogManager.getLogger(AccountController.class);
  private List<TransferListListener> transferListListeners = new ArrayList<TransferListListener>();
  private ExecutorService executorService;
  public static int transferCount = 10000;

  public AccountControllerTransferMoneyExecutor(ExecutorService executorService) {
    this.executorService = executorService;
    initMockData();
  }

  // Auto Accounts initialize for Multithreaded API
  @Override
  public void initMockData(){

    Map<Integer, Account> accountList = new ConcurrentHashMap<>(100);
    accountList.put(1, new Account(1, new BigDecimal(1000000)));
    accountList.put(2, new Account(2, new BigDecimal(6000)));
    accountList.put(3, new Account(3, new BigDecimal(9000)));
    accountList.put(4, new Account(4, new BigDecimal(7000000)));
    accountList.put(5, new Account(5, new BigDecimal(8000)));
    accountList.put(6, new Account(6, new BigDecimal(1000000)));
    accountList.put(7, new Account(7, new BigDecimal(2000)));
    accountList.put(8, new Account(8, new BigDecimal(3000)));
    accountList.put(9, new Account(9, new BigDecimal(4000000)));

    for (int i = 10; i <= transferCount; i++) {
      accountList.put(i, new Account(i, new BigDecimal(10000)));
    }

    AccountRepository.getInstace().setAccountList(accountList);

    logger.debug( String.format("accountList mock data initialized with size: %d", transferCount) );
  }

  public void transferRandomList(){

    for (int i = 1; i <= transferCount; i++) {

      int sendAccountId = ThreadLocalRandom.current().nextInt(1, transferCount);
      int receiveAccountId = ThreadLocalRandom.current().nextInt(1, transferCount);

      if(sendAccountId == receiveAccountId)
        receiveAccountId = (receiveAccountId+1) % transferCount;

      Account sendAccount = getAccount(sendAccountId);
      Account receiveAccount = getAccount(receiveAccountId);

      transfer(sendAccount, receiveAccount);
    }

    closeExecutor();
  }

  public void transfer(int sendAccountId, int receiveAccountId) {
    Account sendAccount = getAccount(sendAccountId);
    Account receiveAccount = getAccount(receiveAccountId);

    if(sendAccountId == receiveAccountId)
      throw new IllegalArgumentException("Same account cannot send to itself.");

    transfer(sendAccount, receiveAccount);

    closeExecutor();
  }

  private void transfer(Account sendAccount, Account receiveAccount) {

    AccountControllerTransferMoneyRunnable accountRunnable = new AccountControllerTransferMoneyRunnable(sendAccount, receiveAccount, this);
    executorService.submit(accountRunnable);
  }

  private void closeExecutor() {

    executorService.shutdown();
    logger.info("ExecutorService shutdown.");

    // Ongoing tasks must be terminated in order to control the results in a health way
    try {
      executorService.awaitTermination(5000, TimeUnit.SECONDS);
      logger.info("ExecutorService terminated.");
      onTransfersCompleted();
    } catch (InterruptedException e) {
      logger.error("ExecutorService terminated.", e);
    }
  }

  public void addListener(TransferListListener listener) {
    transferListListeners.add(listener);
  }
  public void removeListener(TransferListListener listener) {
    transferListListeners.remove(listener);
  }

  @Override
  public void onTransferStart(Account sendAccount, Account receiveAccount) {
    for (TransferListListener transfer: transferListListeners) {
      transfer.onTransferStart(sendAccount, receiveAccount);
    }
  }

  @Override
  public void onTransferCompleted(Account sendAccount, Account receiveAccount) {
    for (TransferListListener transfer: transferListListeners) {
      transfer.onTransferCompleted(sendAccount, receiveAccount);
    }
  }

  @Override
  public void onTransfersCompleted(){
    for (TransferListListener transfer: transferListListeners) {
      transfer.onTransfersCompleted();
    }
  }

  private Account getAccount(Integer id){

    return AccountRepository.getInstace().getAccount(id);
  }
}